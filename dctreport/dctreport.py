import codecs

def change_report_for_dct(f):
    # Read in the file
    with codecs.open(f, 'r',encoding='utf8') as file :
        filedata = file.read()

    # Replace the target string
    filedata = filedata.replace('addSummary(topsuite);', 'dctSummary();')
    filedata = filedata.replace('addStatistics();', 'dctStatistics();')

    idx = filedata.index('function addStatistics() {')

    dctScript = """
function dctTest(suiteFullName) {
    if(['DCT-14880', 'DCT-14882', 'DCT-14883', 'DCT-14884', 'DCT-14886'].some(function(e){ return suiteFullName.includes(e);})) {
        sub = suiteFullName.substring(suiteFullName.lastIndexOf('DCT-')).match(/\./g);
        if(sub != null && sub.length == 1) {
            return true
        }
    }
    return false
}

function endToEndTest(suiteFullName) {
    if(suiteFullName.includes('.End-to-end test.')) {
        sub = suiteFullName.substring(suiteFullName.lastIndexOf('.End-to-end test.')).match(/\./g);
        if(sub != null && sub.length == 3) {
            return true
        }
    }
    return false
}

function regressionTest(suiteFullName) {
    if(suiteFullName.includes('Regression Test')) {
        sub = suiteFullName.substring(suiteFullName.lastIndexOf('Regression Test')).match(/\./g);
        if(sub != null && sub.length == 1) {
            return true
        }
    }
    return false
}

function dctSummary() {
    failCount = 0
    for(suite of window.testdata.statistics()['suite']) {
        tokens = suite.fullName.split('.')
        if(dctTest(suite.fullName) || endToEndTest(suite.fullName) || regressionTest(suite.fullName))
            if(suite.fail > 0)
                failCount = failCount + 1
    }
    var opts = {logURL: window.settings.logURL};
    var topsuite = window.testdata.suite();
    topsuite.criticalFailed = failCount
    dctSummary = {criticalFailed: failCount, times: topsuite.times};
    $.tmpl('summaryTableTemplate', topsuite, opts).insertAfter($('#header'));
}

function dctStatistics() {
    $('<div id="dct-statistics-container"><h2>dcTrack Statistics</h2></div>').insertBefore('#statistics-container')
    util.map(['DCT-14880', 'DCT-14882', 'DCT-14883', 'DCT-14884', 'DCT-14886'], addDctStatTable);
    util.map([{name:'Add an Item', id:'add-an-item'}, {name:'Capacity', id:'capacity'}, {name:'Change Management', id:'change-management'}, {name:'Plan to decomm', id:'plan-to-decomm'}], addEndToEndStatTable);
    util.map([{name:'Regression Test', id:'regression-test'}], addRegressionStatTable);
    addTooltipsToElapsedTimes();
    enableStatisticsSorter();
}

function addDctStatTable(tableName) {
    var stats = window.testdata.statistics()['suite'].slice();
    var statHeaders =
        '<th class="stats-col-stat">Total</th>' +
        '<th class="stats-col-stat">Pass</th>' +
        '<th class="stats-col-stat">Fail</th>' +
        '<th class="stats-col-elapsed">Elapsed</th>' +
        '<th class="stats-col-graph">Pass / Fail</th>';
    var statTable =
        '<table class="statistics" id="' + tableName + '-stats"><thead><tr>' +
        '<th class="stats-col-name">' + tableName + ' Statistics</th>' + statHeaders +
        '</tr></thead></table>';
    var testCount = 0;
    var tbody = $('<tbody></tbody>');
    for (var i = 0, len = stats.length; i < len; i++) {
        if(dctTest(stats[i].fullName) && stats[i].fullName.includes(tableName)) {
            stats[i].fullName = stats[i].fullName.substring(stats[i].fullName.lastIndexOf('.')+1)
            $.tmpl('suiteStatisticsRowTemplate', stats[i], {index: i}).appendTo(tbody);
            testCount = testCount + 1;
        }
    }
    if(testCount == 0)
        return;
    $(statTable).appendTo('#dct-statistics-container');
    tbody.appendTo('#' + tableName + '-stats');
}

function addEndToEndStatTable(table) {
    var stats = window.testdata.statistics()['suite'].slice();
    var statHeaders =
        '<th class="stats-col-stat">Total</th>' +
        '<th class="stats-col-stat">Pass</th>' +
        '<th class="stats-col-stat">Fail</th>' +
        '<th class="stats-col-elapsed">Elapsed</th>' +
        '<th class="stats-col-graph">Pass / Fail</th>';
    var statTable =
        '<table class="statistics" id="' + table['id'] + '-stats"><thead><tr>' +
        '<th class="stats-col-name">' + table['name'] + ' Statistics</th>' + statHeaders +
        '</tr></thead></table>';
    var testCount = 0;
    var tbody = $('<tbody></tbody>');
    for (var i = 0, len = stats.length; i < len; i++) {
        if(endToEndTest(stats[i].fullName) && stats[i].fullName.includes(table['name'])) {
            stats[i].fullName = stats[i].fullName.substring(stats[i].fullName.lastIndexOf('.')+1)
            $.tmpl('suiteStatisticsRowTemplate', stats[i], {index: i}).appendTo(tbody);
            testCount = testCount + 1;
        }
    }
    if(testCount == 0)
        return;
    $(statTable).appendTo('#dct-statistics-container');
    tbody.appendTo('#' + table['id'] + '-stats');
}

function addRegressionStatTable(table) {
    var stats = window.testdata.statistics()['suite'].slice();
    var statHeaders =
        '<th class="stats-col-stat">Total</th>' +
        '<th class="stats-col-stat">Pass</th>' +
        '<th class="stats-col-stat">Fail</th>' +
        '<th class="stats-col-elapsed">Elapsed</th>' +
        '<th class="stats-col-graph">Pass / Fail</th>';
    var statTable =
        '<table class="statistics" id="' + table['id'] + '-stats"><thead><tr>' +
        '<th class="stats-col-name">' + table['name'] + ' Statistics</th>' + statHeaders +
        '</tr></thead></table>';
    var tbody = $('<tbody></tbody>');
    var testCount = 0;
    for (var i = 0, len = stats.length; i < len; i++) {
        tokens = stats[i].label.split('.')
        if(regressionTest(stats[i].fullName) && stats[i].fullName.includes(table['name'])) {
            stats[i].fullName = stats[i].fullName.substring(stats[i].fullName.lastIndexOf('.')+1)
            $.tmpl('suiteStatisticsRowTemplate', stats[i], {index: i}).appendTo(tbody);
            testCount = testCount + 1;
        }
    }
    if(testCount == 0)
        return;
    $(statTable).appendTo('#dct-statistics-container');
    tbody.appendTo('#' + table['id'] + '-stats');
}
    """
    filedata = filedata[:idx] + dctScript + filedata[idx:]

    # Write the file out again
    with codecs.open(f, 'w',encoding='utf8') as file:
        file.write(filedata)

if __name__ == '__main__':
    """
    >>> DctReport.py report.html log.html
    """
    import sys
    for arg in sys.argv[1:]:
        change_report_for_dct(arg)
